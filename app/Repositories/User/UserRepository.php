<?php


namespace App\Repositories\User;

use App\Models\User;

class UserRepository implements IUserInterface
{
    protected  $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function all()
    {
        return $this->user::all();
    }

    public function find(int $user_id)
    {
        return $this->user::find($user_id);
    }

    public function delete(int $user_id)
    {
        $this->user::destroy($user_id);
    }

    public function update(int $user_id, array $user_data)
    {
        $find = $this->user::find($user_id);
        return $find->update($user_data);
    }

    public function create(array $user_data)
    {
        $this->user::create($user_data);
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->user, $method], $args);
    }
}