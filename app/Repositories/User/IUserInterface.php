<?php


namespace App\Repositories\User;


interface IUserInterface
{
    public function all();
    public function find( int $user_id);
    public function delete( int $user_id);
    public function update( int $user_id, array $user_data);
    public function create( array $user_data);
}