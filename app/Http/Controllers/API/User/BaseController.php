<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    protected $repository ;
    protected $data;
    public function __construct()
    {
        $this->data = [];
    }
}
