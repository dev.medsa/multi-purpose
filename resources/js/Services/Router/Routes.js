import Dashboard from '../../components/Dashboard.vue';
import Profile from '../../components/Profile.vue';
import Users from '../../components/User/Users.vue';
const allURL =[
    { path: '/profile', component: Profile ,name:'Profile' },
    { path: '/dashboard', component: Dashboard ,name:'Dashboard' },
    { path: '/users', component: Users ,name:'Users' },
]
export default allURL;