<!-- Main Footer -->
<footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-sm-none d-md-block">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>CopyLeft &copy; 2018 <a href="https://gitlab.com/maz.develop/">Mr.Mohsen Alizadeh</a>.</strong>
    </footer>
</div>
<script src="{{ asset('js/admin.js') }}"></script>
</body>

</html>