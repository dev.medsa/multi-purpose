
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="index3.html" class="brand-link">
                    <span class="brand-text font-weight-light d-flex p-4">پنل مدیریت</span>
                </a>
    
                <!-- Sidebar -->
                <div class="sidebar">
                    <div>
                        <!-- Sidebar user panel (optional) -->
                        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                            <div class="image">
                                <img src="https://scontent-iad3-1.cdninstagram.com/vp/e499f8fa6f1c927f0938b58ccb759c20/5E333C6D/t51.2885-19/66126083_883580348669396_9206233347817209856_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com" class="img-circle elevation-2" alt="User Image">
                            </div>
                            <div class="info">
                                <a href="#" class="d-block">رامین</a>
                            </div>
                        </div>
    
                        <!-- Sidebar Menu -->
                        <nav class="mt-2">
                            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-dashboard green"></i>
                                        <p>
                                            داشبورد
                                            <i class="fa fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        
                                        <li class="nav-item">
                                            <router-link to="/dashboard" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>آیکون‌ها</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/UI/buttons.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>دکمه‌ها</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/UI/sliders.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>اسلایدر‌ها</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-cog indigo"></i>
                                        <p>
                                            مدیریت
                                            <i class="right fa fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="pages/charts/chartjs.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>نمودار ChartJS</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/charts/flot.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>نمودار Flot</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/charts/inline.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>نمودار Inline</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-user-circle-o blue"></i>
                                        <p>
                                            پروفایل
                                            <i class="right fa fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        
                                        <li class="nav-item">
                                            <router-link to="/profile" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>پروفایل</p>
                                            </router-link>
                                        </li>
                                        <li class="nav-item">
                                            <router-link to="/users" class="nav-link">
                                                <i class="fa fa-users nav-icon"></i>
                                                <p>کاربران</p>
                                            </router-link>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-edit orange"></i>
                                        <p>
                                            فرم‌ها
                                            <i class="fa fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="pages/forms/general.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>اجزا عمومی</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/forms/advanced.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>پیشرفته</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/forms/editors.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>ویرایشگر</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon fa fa-file-image-o teal"></i>
                                        <p>
                                            گالری
                                            <i class="fa fa-angle-left right"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <a href="pages/tables/simple.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>جداول ساده</p>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pages/tables/data.html" class="nav-link">
                                                <i class="fa fa-circle-o nav-icon"></i>
                                                <p>جداول داده</p>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item has-treeview align-content-center">
                                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); 
                                        document.getElementById('logout-form').submit();">
                                        <i class="nav-icon fa fa-sign-out red"></i>
                                        <p>
                                            {{ __('خروج') }}
                                        </p>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </nav>
                        <!-- /.sidebar-menu -->
                    </div>
                </div>
                <!-- /.sidebar -->
            </aside>