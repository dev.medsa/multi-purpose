<!--------------------footer section starts here--------------------->
    <div class="footer">
        <div class="container">
            <br>
            <div class="hr">
                <div class="row">

                </div>
            </div>
            <br><br>
            <div class="info">
                <div class="row">
                    <div class="col-lg-4" id="personal">
                        <p class="wow fadeInUp">پست الکترونیکی</p>
                        <h4 class="wow fadeInUp" data-wow-delay="0.3s">ramin@gmail.com</h4>
                        <br><br>
                    </div>
                    <div class="col-lg-4" id="media">
                        <p class="wow fadeInUp" data-wow-delay="0.0s">دنبال کنید</p>
                        <ul>
                            <li id="in" class="wow fadeInUp" data-wow-delay="0.4s"><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li id="ln" class="wow fadeInUp" data-wow-delay="0.6s"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li id="tw" class="wow fadeInUp" data-wow-delay="0.8s"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li id="te" class="wow fadeInUp" data-wow-delay="1s"><a href="#"><i class="fa fa-telegram"></i></a></li>
                        </ul>
                        <br><br>
                    </div>
                    <div class="col-lg-4" id="personal">
                        <p class="wow fadeInUp" data-wow-delay="0.0s"> :ساخته شده توسط </p>
                        <h4 class="wow fadeInUp" data-wow-delay="0.3s">medsa</h4>
                        <br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--------------------footer section ends here----------------------->

