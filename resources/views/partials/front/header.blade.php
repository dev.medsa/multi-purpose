 <!-----------------------navbar start here------------------------>
    <div class="hero-section">
        <div class="menu-btn">
            <i class="fa fa-bars"></i>
        </div>
        <div class="brand">
            <p><a href="#">RamoDecor</a></p>
        </div>
    </div>
    <div class="menu">
        <div class="close-menu">
            <i class="fa fa-times"></i>
        </div>
        <div class="nav">
            <ul class="main-menu">
                <li class="main-menu-item"><a href="#">خانه اصلی</a></li>
                <li class="main-menu-item"><a href="#">وبلاگ</a></li>
                <li class="main-menu-item">
                    <a href="#">نمونه کارها &nbsp;<i class="fa fa-plus"></i></a>
                    <ul class="submenu">
                        <li class="submenu-item"><a href="#">دکوراسیون داخلی</a></li>
                        <li class="submenu-item"><a href="#">تعمیرات مبلمان</a></li>
                    </ul>
                </li>
                <li class="main-menu-item"><a href="#">ارتباط با ما</a></li>
            </ul>
        </div>
        <div class="contact">
            <p class="call"><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;+989222362414</p>
            <p class="mail"><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;</p>
        </div>
        <div class="media">
            <ul>
                <li><a href="#">Linkdin</a></li>
                <li><a href="#">Tweeter</a></li>
                <li><a href="#">instagram</a></li>
                <li><a href="#">telegram</a></li>
            </ul>
        </div>
    </div>
    <!-----------------------navbar ends here-------------------------->