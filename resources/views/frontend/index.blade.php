@extends('layouts.frontend')
    @section('content')
        <!--------------------hero section starts here---------------------->
    <div class="hero" id="hero">
        <h1>RamoDecor</h1>
    </div>
    <div class="scroll-down"></div>
    <br><br>
    <div class="scroll-down-2"></div>
    <!--------------------hero section ends here----------------------->

    <!------------------sample section starts here--------------------->
        <div class="container-fluid">
            <br><br><br>
            <h6>نمونه کارها</h6>
            <div class="vertical"></div>
            <br>
            <div class="whitespace"></div>
            <div class="whitespace"></div>
            <div class="row anim" >
                <div class="col-lg-8">
                    <div class="text-project anim">
                        <h1>دکوراسیون داخلی</h1>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                        <div class="bttn">
                            <button class="btn btn-outline-warning">بیشتر ببینید</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 project  anim" >
                    <div class="imgBox">
                        <a href="">
                            <img src="/images/03.jpg" alt="" class="picture" data-wow-delay="0.1s">
                            <div class="detail">
                                <div class="contentive">
                                    <h2>دکوراسیون داخلی</h2>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="whitespace"></div>
            <div class="row anim">
                <div class="col-lg-6 project  anim">
                    <div class="imgBox">
                        <a href="">
                            <img src="/images/04.jpg" alt="" class="picture" data-wow-delay="0.1s">
                            <div class="detail">
                                <div class="contentive">
                                    <h2>تعمیرات مبلمان</h2>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-project">
                        <h1>تعمیرات مبلمان</h1>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                        <div class="bttn">
                            <button class="btn btn-outline-warning">بیشتر ببینید</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="whitespace"></div>
            
        </div>
            <div class="container">
                <br><br><br>
                <h6 class="anim">بلاگ</h6>
                <div class="vertical"></div>
                <br>
                <div class="whitespace"></div>
                <div class="whitespace"></div>
                <section class="center slider anim">
                    <div class="blogContent">
                        <img src="/images/02.jpg" alt="">
                                <h3>یکی از صد ها</h3>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                <p>نوشته شده :<a href="#">شیخ رامین</a> | <a href="#">تاریخ</a></p>
                                <a href="#" class="btn">بیشتر بخوانید</a>
                    </div>
                    <div class="blogContent">
                        <img src="/images/03.jpg" alt="">
                                <h3>یکی از صد ها</h3>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                <p>نوشته شده :<a href="#">شیخ رامین</a> | <a href="#">تاریخ</a></p>
                                <a href="#" class="btn">بیشتر بخوانید</a>
                    </div>
                    <div class="blogContent">
                        <img src="/images/04.jpg" alt="">
                            <h3>یکی از صد ها</h3>
                            <p>نوشته شده :<a href="#">شیخ رامین</a> | <a href="#">تاریخ</a></p>
                            <a href="#" class="btn">بیشتر بخوانید</a>
                    </div>
                    <div class="blogContent">
                        <img src="/images/05.jpg" alt="">
                            <h3>یکی از صد ها</h3>
                            <p>نوشته شده :<a href="#">شیخ رامین</a> | <a href="#">تاریخ</a></p>
                            <a href="#" class="btn">بیشتر بخوانید</a>
                    </div>
                </section>
                <div class="whitespace"></div>
                <div class="collab anim">
                    <div class="row">
                        <div class="col-lg-1">
                            <button class="btn btn-outline-warning">مشاهده بیشتر</button>
                        </div> 
                    </div>
                </div>
            </div>
            
            
    <!--------------------sample section ends here----------------------->
    @endsection