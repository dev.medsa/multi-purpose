<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src = "{{ asset('js/app.js') }}" ></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono&display=swap" rel="stylesheet">
</head>

<body>

    {{-- @if (Route::has('login'))
    <div class="top-right links">
        @auth
        <a href="{{ url('/home') }}">Home</a> @else
        <a href="{{ route('login') }}">Login</a> @if (Route::has('register'))
        <a href="{{ route('register') }}">Register</a> @endif @endauth
    </div>
    @endif --}}
<div class="wrapper">
    @include('partials.front.header')
    @yield('content')
    @include('partials.front.footer')
</div>
<script type="text/javascript">
    $('ul.main-menu li').click(function(e) {
        e.preventDefault();
        if ($(this).siblings('li').find('ul.submenu:visible').length) {
            $('ul.submenu').slideUp('normal');
        }
        $(this).find('ul.submenu').slideToggle('normal');
    });
    var t1 = new TimelineMax({paused:true});
    t1.to(".menu",0.8,{
        autoAlpha:1
    });

    t1.staggerFrom(".main-menu li a:not(.submenu li a)",1 , {
        opacity:0,
        y:10,
        ease:Power3.easeInOut
    },0.1);

    t1.from(".submenu",0.8 ,{
        autoAlpha:0
    });

    t1.staggerFrom(".media ul li",1 , {
        opacity:0,
        y:10,
        ease:Power3.easeInOut
    },0.1,"-=2");

    t1.from(".call",1,{
        delay:-2,
        opacity:0,
        y:10,
        ease:Power3.easeInOut
    });
    t1.from(".mail",1,{
        delay:-1.6,
        opacity:0,
        y:10,
        ease:Power3.easeInOut
    });

    t1.reverse();

    $(document).on("click",".menu-btn",function() {
        t1.reversed(!t1.reversed());
    });

    $(document).on("click",".close-menu",function() {
        t1.reversed(!t1.reversed());
    });
    window.wow.init();
    new window.hoverEffect({
        parent: document.querySelector('.hero'),
        image1: "/images/decor.jpg",
        image2: "/images/decor-2.jpg",
        displacementImage: '/images/heightMap.png'
    })
    $(document).on('ready',function () {
        $(".center").slick({
            dots:false,
            infinite:false,
            centerMode:true,
            slidesToShow:2,
            slidesToScroll:1,
            arrows:false,
            responsive:[{
                breakpoint:1024,
                settings:{
                    slidesToShow:2,
                    infinite:true
                }
            }
            ,{
                breakpoint:768,
                settings:{
                    slidesToShow:1,
                    infinite:true
                }
            }
            ,{
                breakpoint:300,
                settings:"unslick"
            }]
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/scrollreveal@4.0.5/dist/scrollreveal.min.js"></script>
<script>
ScrollReveal().reveal('.anim');
</script>
</body> 
</html>